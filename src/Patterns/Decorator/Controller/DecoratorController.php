<?php

namespace Patterns\Decorator\Controller;

use App\View\PatternView;
use Patterns\Decorator\Example\DecoratorExample;

class DecoratorController
{
    const NAME = 'Decorator';
    const DESCRIPTION = 'Decorator (Декоратор)';
    const CATEGORY = 'Structural';

    public function exec()
    {
        $pattern = new DecoratorExample();
        $pattern->setMessage('Описание паттерна '.$this::NAME, 'h4');
        $pattern->run();

        $view = new PatternView();
        $view->setBody($pattern->getMessage())->setTitle('Singleton')->render();
    }
}
