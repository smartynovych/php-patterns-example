<?php

namespace Patterns\Prototype\Example\Resources;

abstract class FigurePrototype
{
    /**
     * @var string
     */
    protected $color;

    /**
     * @var int
     */
    protected $type;

    abstract public function __clone();

    public function getColor(): string
    {
        return $this->color;
    }

    public function setColor(string $color)
    {
        $this->color = $color;
    }

    public function getType()
    {
        return $this->type;
    }
}
