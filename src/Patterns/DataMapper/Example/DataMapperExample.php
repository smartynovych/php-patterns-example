<?php

namespace Patterns\DataMapper\Example;

use App\Services\PatternDescriptionServices;
use Patterns\DataMapper\Example\Resources\StorageAdapter;
use Patterns\DataMapper\Example\Resources\UserMapper;

class DataMapperExample extends PatternDescriptionServices
{
    public function run()
    {
        $storage = new StorageAdapter([1 => ['username' => 'domnikl', 'email' => 'liebler.dominik@gmail.com']]);
        $mapper = new UserMapper($storage);

        $user = $mapper->findById(1);
    }
}
