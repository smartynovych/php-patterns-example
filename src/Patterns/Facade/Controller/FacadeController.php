<?php

namespace Patterns\Facade\Controller;

use App\View\PatternView;
use Patterns\Facade\Example\FacadeExample;

class FacadeController
{
    const NAME = 'Facade (Фасад)';
    const DESCRIPTION = 'Facade (Фасад). Структурный шаблон проектирования, позволяющий скрыть сложность системы 
    путём сведения всех возможных внешних вызовов к одному объекту, 
    делегирующему их соответствующим объектам системы.';
    const CATEGORY = 'Structural';
    const WIKI = 'https://ru.wikipedia.org/wiki/%D0%A4%D0%B0%D1%81%D0%B0%D0%B4_(%D1%88%D0%B0%D0%B1%D0%BB%D0%BE%D0%BD_%D0%BF%D1%80%D0%BE%D0%B5%D0%BA%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D1%8F)';

    public function exec()
    {
        $pattern = new FacadeExample();
        $pattern->setMessage($this::WIKI, 'wiki');
        $pattern->setMessage('Описание паттерна '.$this::NAME, 'h4');
        $pattern->setMessage($this::DESCRIPTION, 'p');
        $pattern->run();

        $view = new PatternView();
        $view->setBody($pattern->getMessage())->setTitle('Singleton')->render();
    }
}
