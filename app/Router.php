<?php

use App\Model\PatternModel;

class Router
{
    public function start()
    {
        try {
            $uri = ltrim($_SERVER['REQUEST_URI'], '/');
            if (strpos($uri, '/')) {
                $uri = substr($uri, 0, strpos($uri, '/'));
            }
            if (strpos($uri, '?')) {
                $uri = substr($uri, 0, strpos($uri, '?'));
            }

            if (strlen($uri) > 0) {
                $patterns = new PatternModel();
                if (isset($patterns->getPattern()[$uri])) {
                    $patternClass = '\\Patterns\\'.$uri.'\\Controller\\'.$uri.'Controller';
                } else {
                    throw new Exception('Pattern not found', 404);
                }
            } else {
                $patternClass = '\\App\\Controller\\PatternController';
            }

            $kernel = new $patternClass();
            $kernel->exec();
        } catch (Exception $e) {
            $kernel = new \App\Controller\PatternController();
            $kernel->error($e->getCode(), $e->getMessage());
        }
    }
}
