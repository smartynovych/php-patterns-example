<?php

namespace App\Controller;

use App\Model\PatternModel;
use App\View\PatternView;

class PatternController extends PatternModel
{
    public function exec()
    {
        $pattern = parent::getPatternByCategory();
        $view = new PatternView();

        $view->setPatternList($pattern)
            ->setTitle('')
            ->render();
    }

    public function error($code, $message)
    {
        switch ($code) {
            case '404':
                $view = new PatternView($code);

                header('HTTP/1.0 404 Not Found');

                $view->setTitle($code.' '.$message)
                    ->setBody($message)
                    ->render();
                break;

            default:
                $view = new PatternView();

                $view->setTitle($code.' '.$message)
                    ->setBody($message)
                    ->render();
                break;
        }
    }
}
