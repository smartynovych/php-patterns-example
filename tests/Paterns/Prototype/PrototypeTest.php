<?php

use PHPUnit\Framework\TestCase;
use Patterns\Prototype\Example\Resources\Ball;
use Patterns\Prototype\Example\Resources\Square;

class PrototypeTest extends TestCase
{
    public function testPrototype()
    {
        $colorList = ['aqua', 'black', 'blue', 'fuchsia', 'gray', 'green',
            'lime', 'maroon', 'navy', 'olive', 'orange', 'purple', 'red',
            'silver', 'teal', 'white', 'yellow', ];

        $ballPrototype = new Ball();
        $squarePrototype = new Square();

        $this->assertInstanceOf(Ball::class, $ballPrototype);
        $this->assertInstanceOf(Square::class, $squarePrototype);
        $this->assertNotSame($ballPrototype, $squarePrototype);

        foreach ($colorList as $color) {
            $ball[$color] = clone $ballPrototype;
            $ball[$color]->setColor($color);

            $square[$color] = clone $squarePrototype;
            $square[$color]->setColor($color);
        }

        $color = 'green';
        $this->assertNotSame($ballPrototype, $ball[$color]);

        $this->assertSame($color, $ball[$color]->getColor());
        $this->assertSame('ball', $ball[$color]->getType());

        $this->assertInstanceOf(Ball::class, $ball[$color]);
        $this->assertInstanceOf(Square::class, $square[$color]);
    }
}
