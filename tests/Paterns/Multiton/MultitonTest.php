<?php

use PHPUnit\Framework\TestCase;
use Patterns\Multiton\Example\Resources\Multiton;

class MultitonTest extends TestCase
{
    public function testMultiton()
    {
        $mysql = Multiton::getInstance(Multiton::MYSQL);
        $this->assertInstanceOf(Multiton::class, $mysql);

        $mysql2 = Multiton::getInstance(Multiton::MYSQL);
        $this->assertSame($mysql, $mysql2);

        $sqlite = Multiton::getInstance(Multiton::SQLITE);
        $this->assertInstanceOf(Multiton::class, $sqlite);

        $this->assertNotSame($mysql, $sqlite);
        $this->assertNotSame($mysql->getConnection(), $sqlite->getConnection());
    }
}
