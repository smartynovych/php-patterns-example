<?php

namespace Patterns\Composite\Example\Resources;

class Infantry implements RenderableInterface
{
    private $count;

    public function __construct(int $count)
    {
        $this->count = $count;
    }

    public function render(): string
    {
        return 'Infantry: '.$this->count.' units';
    }
}
