<?php

namespace Patterns\Bridge\Example\Resources;

abstract class DrinkAbstract
{
    /**
     * @var SupplementsInterface
     */
    protected $implementation;

    /**
     * @param SupplementsInterface $supplement
     */
    public function __construct(SupplementsInterface $supplement)
    {
        $this->implementation = $supplement;
    }

    /**
     * @param SupplementsInterface $supplement
     */
    public function setImplementation(SupplementsInterface $supplement)
    {
        $this->implementation = $supplement;
    }

    abstract public function get();
}
