<?php

namespace Patterns\Composite\Example;

use App\Services\PatternDescriptionServices;
use Patterns\Composite\Example\Resources\Sheep;
use Patterns\Composite\Example\Resources\Army;
use Patterns\Composite\Example\Resources\Cavalry;
use Patterns\Composite\Example\Resources\Infantry;

class CompositeExample extends PatternDescriptionServices
{
    public function run()
    {
        $this->setMessage('Создаем армию', 'strong');
        $this->setMessage('$army = new Army();', 'code');
        $army = new Army();

        $this->setMessage('Нанимаем кавалерию и пехоту', 'i');
        $this->setMessage('$army->addElement(new Cavalry(1000));', 'code');
        $this->setMessage('$army->addElement(new Infantry(5000));', 'code');
        $army->addElement(new Cavalry(1000));
        $army->addElement(new Infantry(5000));

        $this->setMessage('Отрисовываем армию', 'i');
        $this->setMessage('$army->render();', 'code');
        $this->setMessage($army->render(), 'i');

        $this->setMessage('Создаем новый объкт Флот', 'i');
        $this->setMessage('$fleet = new Army();', 'code');
        $fleet = new Army();

        $this->setMessage('Покупаем корабли для перемещения армии', 'i');
        $this->setMessage('$fleet->addElement(new Sheep(50));', 'code');
        $fleet->addElement(new Sheep(50));

        $this->setMessage('Объединяем флот с армией', 'i');
        $this->setMessage(' $army->addElement($fleet);', 'code');
        $army->addElement($fleet);

        $this->setMessage('Дальше работаем с иерархией объектов как с одним экземпляторм Армия', 'i');
        $this->setMessage('$army->render();', 'code');
        $this->setMessage($army->render(), 'i');
    }
}
