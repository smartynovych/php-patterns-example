<?php

namespace Patterns\Multiton\Controller;

use App\View\PatternView;
use Patterns\Multiton\Example\MultitonExample;

class MultitonController
{
    const NAME = 'Multiton';
    const DESCRIPTION = 'Multiton (Пул одиночек)';
    const CATEGORY = 'Creational';

    public function exec()
    {
        $pattern = new MultitonExample();
        $pattern->setMessage('Описание паттерна '.$this::NAME, 'h4');
        $pattern->run();
        $view = new PatternView();
        $view->setBody($pattern->getMessage())->setTitle('Singleton')->render();
    }
}
