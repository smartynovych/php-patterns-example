<?php

namespace Patterns\Adapter\Example\Resources;

interface CrudInterface
{
    public function create(): void;

    public function read(): ?int;

    public function update(): void;

    public function delete(): void;
}
