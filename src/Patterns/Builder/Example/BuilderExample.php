<?php

namespace Patterns\Builder\Example;

use App\Services\PatternDescriptionServices;
use Patterns\Builder\Example\Resources\FarmerlandBuilder;
use Patterns\Builder\Example\Resources\Lordship;
use Patterns\Builder\Example\Resources\WarlandBuilder;

class BuilderExample extends PatternDescriptionServices
{
    public function run()
    {
        $this->setMessage('Создаем объект Lordship (Director)', 'strong');
        $this->setMessage('$newCountry = new Lordship();', 'code');
        $newCountry = new Lordship();

        $this->setMessage('Создаем объект определенной страны Warland (ConcreteBuilder)', 'strong');
        $this->setMessage('$warland = new WarlandBuilder();', 'code');
        $warland = new WarlandBuilder();

        $this->setMessage('Создаем объект другой определенной страны Farmerland (ConcreteBuilder)', 'strong');
        $this->setMessage('$farmerland = new FarmerlandBuilder();', 'code');
        $farmerland = new FarmerlandBuilder();

        $this->setMessage('Строим страну Warland', 'strong');
        $this->setMessage('$newCountry->build($warland);', 'code');
        $this->setMessage($newCountry->build($warland));

        $this->setMessage('Строим страну Farmerland', 'strong');
        $this->setMessage('$newCountry->build($farmerland);', 'code');
        $this->setMessage($newCountry->build($farmerland));
    }
}
