<?php

namespace Patterns\Builder\Example\Resources;

abstract class CountryBuilder
{
    protected $country;

    public function createCountry()
    {
        $this->country = new Country();
    }

    abstract public function addName();

    abstract public function addPopulation();

    abstract public function addLanguage();

    abstract public function addFeature();

    public function getCountry()
    {
        return $this->country;
    }
}
