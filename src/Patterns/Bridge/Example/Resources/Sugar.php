<?php

namespace Patterns\Bridge\Example\Resources;

class Sugar implements SupplementsInterface
{
    public function create(string $name)
    {
        return $name.' with sugar';
    }
}
