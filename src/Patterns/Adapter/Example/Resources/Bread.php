<?php

namespace Patterns\Adapter\Example\Resources;

/**
 * Class Bread.
 */
class Bread implements BreadInterface
{
    /**
     * @var null
     */
    private $bit = null;

    /**
     * @return int|null
     */
    public function browse(): ?int
    {
        echo $this->bit;

        return $this->bit;
    }

    /**
     * @return int|null
     */
    public function read(): ?int
    {
        return $this->bit;
    }

    public function edit(): void
    {
        $this->bit = 1;
    }

    public function add(): void
    {
        $this->bit = 0;
    }

    public function delete(): void
    {
        $this->bit = null;
    }
}
