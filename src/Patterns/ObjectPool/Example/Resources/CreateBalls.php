<?php

namespace Patterns\ObjectPool\Example\Resources;

class CreateBalls
{
    private $colorList = [
        'aqua', 'black', 'blue', 'fuchsia', 'gray', 'green',
        'lime', 'maroon', 'navy', 'olive', 'orange', 'purple', 'red',
        'silver', 'teal', 'white', 'yellow', ];

    private $color;
    private $position;
    private $diagonal;

    public function __construct()
    {
        $this->color = $this->colorList[array_rand($this->colorList)];
        $this->position = mt_rand(10, 600);
        $this->diagonal = mt_rand(30, 50);
    }

    public function getColor()
    {
        return $this->color;
    }

    /**
     * @return int
     */
    public function getPosition(): int
    {
        return $this->position;
    }

    /**
     * @return int
     */
    public function getDiagonal(): int
    {
        return $this->diagonal;
    }
}
