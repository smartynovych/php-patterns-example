<?php

namespace Patterns\Decorator\Example\Resources;

class WindowsHorizontalScroll extends WindowsDecorator
{
    public function renderWindows(): string
    {
        $data = $this->wrapped->renderWindows();

        return $data.' with horizontal scroll';
    }
}
