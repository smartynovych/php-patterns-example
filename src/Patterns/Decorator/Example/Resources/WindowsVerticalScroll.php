<?php

namespace Patterns\Decorator\Example\Resources;

class WindowsVerticalScroll extends WindowsDecorator
{
    public function renderWindows(): string
    {
        $data = $this->wrapped->renderWindows();

        return $data.' with vertical scroll';
    }
}
