<?php

use PHPUnit\Framework\TestCase;
use Patterns\Decorator\Example\Resources\Windows;
use Patterns\Decorator\Example\Resources\WindowsInterface;
use Patterns\Decorator\Example\Resources\WindowsDecorator;
use Patterns\Decorator\Example\Resources\WindowsHorizontalScroll;
use Patterns\Decorator\Example\Resources\WindowsVerticalScroll;

class DecoratorTest extends TestCase
{
    public function testDecorator()
    {
        $windows = new Windows('Explorer windows');
        $this->assertInstanceOf(WindowsInterface::class, $windows);
        $this->assertEquals('Explorer windows', $windows->renderWindows());

        $windows = new WindowsVerticalScroll($windows);
        $this->assertInstanceOf(WindowsDecorator::class, $windows);
        $this->assertInstanceOf(WindowsInterface::class, $windows);
        $this->assertEquals('Explorer windows with vertical scroll', $windows->renderWindows());

        $windows = new WindowsHorizontalScroll($windows);
        $this->assertInstanceOf(WindowsDecorator::class, $windows);
        $this->assertInstanceOf(WindowsInterface::class, $windows);
        $this->assertEquals('Explorer windows with vertical scroll with horizontal scroll', $windows->renderWindows());
    }
}
