<?php

namespace Patterns\Composite\Example\Resources;

interface RenderableInterface
{
    public function render(): string;
}
