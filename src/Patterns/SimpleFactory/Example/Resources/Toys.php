<?php

declare(strict_types=1);

namespace Patterns\SimpleFactory\Example\Resources;

class Toys
{
    public function Lego(string $type = ''): string
    {
        return 'LEGO'.$type;
    }

    public function TeddyBear(string $type = ''): string
    {
        return 'TEDDY-BEAR'.$type;
    }
}
