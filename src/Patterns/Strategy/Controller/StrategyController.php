<?php

namespace Patterns\Strategy\Controller;

use App\View\PatternView;

class StrategyController
{
    const NAME = 'Strategy';
    const DESCRIPTION = 'Strategy';
    const CATEGORY = 'Behavioral';

    public function exec()
    {
        $view = new PatternView();
        $view->setBody(self::DESCRIPTION)->setTitle('Singleton')->render();
    }
}
