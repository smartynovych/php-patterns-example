<?php

namespace Patterns\FactoryMethod\Controller;

use App\View\PatternView;
use Patterns\FactoryMethod\Example\FactoryMethodExample;

class FactoryMethodController
{
    const NAME = 'FactoryMethod';
    const DESCRIPTION = 'Factory Method';
    const CATEGORY = 'Creational';

    public function exec()
    {
        $pattern = new FactoryMethodExample();
        $pattern->setMessage('Описание паттерна '.$this::NAME, 'h4');
        $pattern->run();
        $view = new PatternView();
        $view->setBody($pattern->getMessage())->setTitle('Singleton')->render();
    }
}
