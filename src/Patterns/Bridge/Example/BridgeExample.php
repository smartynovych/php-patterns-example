<?php

namespace Patterns\Bridge\Example;

use App\Services\PatternDescriptionServices;
use Patterns\Bridge\Example\Resources\Tea;
use Patterns\Bridge\Example\Resources\Cafe;
use Patterns\Bridge\Example\Resources\Sugar;
use Patterns\Bridge\Example\Resources\Milk;

class BridgeExample extends PatternDescriptionServices
{
    public function run()
    {
        $this->setMessage('Делаем чай с сахаром, берем сахар', 'strong');
        $this->setMessage('$sugar = new Sugar();', 'code');
        $sugar = new Sugar();

        $this->setMessage('Завариваем чай, наливаем его в чашку и бросаем сахар', 'i');
        $this->setMessage('$tea = new Tea($sugar);', 'code');
        $tea = new Tea($sugar);

        $this->setMessage('И получаем', 'i');
        $this->setMessage('$tea->get();', 'code');
        $this->setMessage($tea->get());

        $this->setMessage('Теперь нас попросили сделать чай с молоком, берем молоко', 'i');
        $this->setMessage('$milk = new Milk();', 'code');
        $milk = new Milk();

        $this->setMessage('Наливаем из того же чайника чай в новую чашку и наливаем молоко', 'i');
        $this->setMessage('$tea->setImplementation($milk);', 'code');
        $tea->setImplementation($milk);

        $this->setMessage('И получаем', 'i');
        $this->setMessage('$tea->get();', 'code');
        $this->setMessage($tea->get());

        $this->setMessage('Темерь мы хотим кофе', 'strong');
        $this->setMessage('Завариваем кофе, наливаем его в чашку и бросаем сахар который остался', 'i');
        $this->setMessage('$cafe = new Cafe($sugar);', 'code');
        $cafe = new Cafe($sugar);

        $this->setMessage('И получаем', 'i');
        $this->setMessage('$cafe->get();', 'code');
        $this->setMessage($cafe->get());
    }
}
