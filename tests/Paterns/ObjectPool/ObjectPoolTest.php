<?php

use PHPUnit\Framework\TestCase;
use Patterns\ObjectPool\Example\Resources\BallsPool;
use Patterns\ObjectPool\Example\Resources\CreateBalls;

class ObjectPoolTest extends TestCase
{
    public function testObjectPool()
    {
        $pool = new BallsPool();
        $ball1 = $pool->get();
        $this->assertInstanceOf(CreateBalls::class, $ball1);

        $ball2 = $pool->get();
        $this->assertNotSame($ball1, $ball2);

        $pool->dispose($ball1);
        $this->assertInstanceOf(CreateBalls::class, $ball1);

        $pool->dispose($ball2);
        $this->assertInstanceOf(CreateBalls::class, $ball2);

        $ball3 = $pool->get();
        $this->assertSame($ball2, $ball3);

        $ball4 = $pool->get();
        $this->assertSame($ball1, $ball4);
    }
}
