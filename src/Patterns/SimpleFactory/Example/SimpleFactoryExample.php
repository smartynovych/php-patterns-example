<?php

namespace Patterns\SimpleFactory\Example;

use Patterns\SimpleFactory\Example\Resources\SimpleFactory;
use App\Services\PatternDescriptionServices;

class SimpleFactoryExample extends PatternDescriptionServices
{
    public function run()
    {
        $this->setMessage('Создаем фабрику', 'p');
        $this->setMessage('$factory = new SimpleFactory()', 'code');

        $factory = new SimpleFactory();

        $toys = $factory->CreateToys();
        $this->setMessage('Производим Игрушки', 'i');
        $this->setMessage('$toys = $factory->CreateToys()', 'code');
        $this->setMessage('$toys->Lego(\' train\')', 'code');
        $this->setMessage($toys->Lego(' train'));
        $this->setMessage('$toys->TeddyBear(\' plush\')', 'code');
        $this->setMessage($toys->TeddyBear(' plush'));

        $books = $factory->CreateBooks();
        $this->setMessage('Производим Книги', 'i');
        $this->setMessage('$books = $factory->CreateBooks()', 'code');
        $this->setMessage('$books->Fantasy(\' for children\')', 'code');
        $this->setMessage($books->Fantasy(' for children'));
        $this->setMessage('$books->Detective(\' for woman\')', 'code');
        $this->setMessage($books->Detective(' for woman'));

        $clothes = $factory->CreateClothes();
        $this->setMessage('Производим Одежду', 'i');
        $this->setMessage('$clothes = $factory->CreateClothes()', 'code');
        $this->setMessage('$clothes->TShirt(\' unisex\')', 'code');
        $this->setMessage($clothes->TShirt(' unisex'));
        $this->setMessage('$clothes->Pants(\' jeans\')', 'code');
        $this->setMessage($clothes->Pants(' jeans'));
    }
}
