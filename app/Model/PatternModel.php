<?php

namespace App\Model;

class PatternModel
{
    public function getPattern()
    {
        $path = __DIR__.'/../../src/Patterns/';
        $dirList = glob($path.'*', GLOB_ONLYDIR);
        $patterns = [];

        foreach ($dirList as $pattern) {
            $className = str_replace($path, '', $pattern);
            $classNsName = 'Patterns\\'.$className.'\\Controller\\'.$className.'Controller';

            if (class_exists($classNsName)) {
                $patterns[$className]['name'] = $classNsName::NAME;
                $patterns[$className]['category'] = $classNsName::CATEGORY;
                $patterns[$className]['description'] = $classNsName::DESCRIPTION;
            }
        }

        return $patterns;
    }

    public function getPatternByCategory()
    {
        $pattern = $this->getPattern();
        $patternByCategory = [];
        foreach ($pattern as $key => $value) {
            $patternByCategory[$value['category']][$key] = $value['name'];
        }

        return $patternByCategory;
    }
}
