<?php

declare(strict_types=1);

namespace Patterns\StaticFactory\Example\Resources;

final class StaticFactory
{
    /**
     * @param string $type
     *
     * @return FactoryInterface
     */
    public static function factory(string $type): FactoryInterface
    {
        switch ($type) {
            case 'toys':
                return new Toys();
                break;

            case 'books':
                return new Books();
                break;

            case 'clothes':
                return new Clothes();
                break;
        }

        throw new \InvalidArgumentException('Unknown format given');
    }
}
