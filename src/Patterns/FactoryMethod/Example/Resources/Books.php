<?php

declare(strict_types=1);

namespace Patterns\FactoryMethod\Example\Resources;

class Books implements ProductProvider
{
    private $name = '';
    private $description = '';
    private $price = '0.00';

    /**
     * @return mixed
     */
    public function getName(): string
    {
        return mb_strtoupper($this->name);
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDescription(): string
    {
        return mb_strtoupper($this->description);
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getPrice(): string
    {
        return '$'.$this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price): void
    {
        $this->price = $price;
    }
}
