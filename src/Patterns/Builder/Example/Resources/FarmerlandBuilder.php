<?php

namespace Patterns\Builder\Example\Resources;

class FarmerlandBuilder extends CountryBuilder
{
    public function addName()
    {
        $this->country->setName('FarmerLand');
    }

    public function addPopulation()
    {
        $this->country->setPopulation(250000);
    }

    public function addLanguage()
    {
        $this->country->setLanguage('farmeries');
    }

    public function addFeature()
    {
        $this->country->setFeature('Farmer');
    }
}
