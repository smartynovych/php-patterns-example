<?php

namespace Patterns\Bridge\Example\Resources;

interface SupplementsInterface
{
    public function create(string $name);
}
