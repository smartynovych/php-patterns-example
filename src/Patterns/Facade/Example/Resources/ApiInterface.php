<?php

namespace Patterns\Facade\Example\Resources;

interface ApiInterface
{
    public function allow(CabinetInterface $cabinet);

    public function deny(CabinetInterface $cabinet);

    public function check();
}
