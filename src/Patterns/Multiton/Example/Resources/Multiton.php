<?php

namespace Patterns\Multiton\Example\Resources;

class Multiton
{
    const MYSQL = 'mysql';
    const SQLITE = 'sqlite';

    /**
     * @var array MultitonExample
     */
    private static $instance = [];

    /**
     * @var string parameters
     */
    private $connection;

    private function __construct($instanceName)
    {
        $this->connection = 'Connect: '.$instanceName;
    }

    public static function getInstance($instanceName): self
    {
        if (!isset(static::$instance[$instanceName])) {
            static::$instance[$instanceName] = new static($instanceName);
        }

        return static::$instance[$instanceName];
    }

    public function getConnection()
    {
        return $this->connection;
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    /*    public function __destruct()
        {
            $this->setParameter([
                'singleton' => null]);
        }*/
}
