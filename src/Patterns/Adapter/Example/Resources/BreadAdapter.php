<?php

namespace Patterns\Adapter\Example\Resources;

/**
 * Class BreadAdapter.
 */
class BreadAdapter implements CrudInterface
{
    /**
     * @var BreadInterface
     */
    protected $bread;

    /**
     * BreadAdapter constructor.
     *
     * @param BreadInterface $bread
     */
    public function __construct(BreadInterface $bread)
    {
        $this->bread = $bread;
    }

    public function create(): void
    {
        $this->bread->add();
    }

    /**
     * @return int|null
     */
    public function read(): ?int
    {
        return $this->bread->read();
    }

    public function update(): void
    {
        $this->bread->edit();
    }

    public function delete(): void
    {
        $this->bread->delete();
    }
}
