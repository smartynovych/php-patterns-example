<?php

declare(strict_types=1);

namespace Patterns\AbstractFactory\Example\Resources;

use Faker\Factory;

class ChildrenBooks extends ProviderBooks
{
    private $name = '';
    private $description = '';
    private $price = '0.00';

    public function __construct(string $name)
    {
        $faker = Factory::create();
        $this->name = $name;
        $this->description = $faker->text;
        $this->price = $faker->randomFloat(2, 1, 100).' '.$faker->currencyCode;
    }

    /**
     * @return mixed
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getPrice(): string
    {
        return $this->price;
    }
}
