<html>
<head>
    <title><?php echo $this->getTitle(); ?></title>
    <link rel="icon" type="image/x-icon" href="/favicon.ico" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="row alert alert-success">
        <div class="col-md-11">
            <h1>PHP Patterns Examples</h1>
        </div>
        <div class="col-md-1">
            <span><a href="/">Home</a></span>
        </div>
    </div>