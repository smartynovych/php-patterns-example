<?php

namespace Patterns\ObjectPool\Controller;

use App\View\PatternView;
use Patterns\ObjectPool\Example\ObjectPoolExample;

class ObjectPoolController
{
    const NAME = 'ObjectPool';
    const DESCRIPTION = 'Object pool (Объектный пул)';
    const CATEGORY = 'Creational';

    public function exec()
    {
        $pattern = new ObjectPoolExample();
        $pattern->setMessage('Описание паттерна '.$this::NAME, 'h4');
        $pattern->run();
        $view = new PatternView();
        $view->setBody($pattern->getMessage())->setTitle('Singleton')->render();
    }
}
