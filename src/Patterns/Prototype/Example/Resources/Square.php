<?php

namespace Patterns\Prototype\Example\Resources;

class Square extends FigurePrototype
{
    protected $type = 'square';

    public function __clone()
    {
    }
}
