<?php

namespace Patterns\Bridge\Example\Resources;

class Tea extends DrinkAbstract
{
    public function get()
    {
        return $this->implementation->create('Tea');
    }
}
