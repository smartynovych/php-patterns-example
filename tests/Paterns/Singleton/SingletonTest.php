<?php

use PHPUnit\Framework\TestCase;
use Patterns\Singleton\Example\ConfigExample;

class SingletonTest extends TestCase
{
    public function testSingleton()
    {
        $config = ConfigExample::getInstance();
        $this->assertInstanceOf(ConfigExample::class, $config);

        $config2 = ConfigExample::getInstance();
        $this->assertInstanceOf(ConfigExample::class, $config2);

        $this->assertSame($config, $config2);
    }
}
