<?php

namespace Patterns\Builder\Example\Resources;

class Country
{
    private $name;
    private $population;
    private $language;
    private $feature;

    /**
     * @param string $name
     *
     * @return Country
     */
    public function setName(string $name): self
    {
        $this->name = mb_strtoupper($name);

        return $this;
    }

    /**
     * @param int $population
     *
     * @return Country
     */
    public function setPopulation(int $population): self
    {
        $this->population = $population;

        return $this;
    }

    /**
     * @param string $language
     *
     * @return Country
     */
    public function setLanguage(string $language): self
    {
        $this->language = mb_strtoupper($language);

        return $this;
    }

    /**
     * @param string $feature
     *
     * @return Country
     */
    public function setFeature(string $feature): self
    {
        $this->feature = mb_strtoupper($feature);

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getPopulation(): int
    {
        return $this->population;
    }

    /**
     * @return string
     */
    public function getLanguage(): string
    {
        return $this->language;
    }

    /**
     * @return string
     */
    public function getFeature(): string
    {
        return $this->feature;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf('Country %s with a population of %d people, the language %s, the main activity - %s', $this->name, $this->population, $this->language, $this->feature);
    }
}
