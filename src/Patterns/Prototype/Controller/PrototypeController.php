<?php

namespace Patterns\Prototype\Controller;

use App\View\PatternView;
use Patterns\Prototype\Example\PrototypeExample;

class PrototypeController
{
    const NAME = 'Prototype';
    const DESCRIPTION = 'Prototype';
    const CATEGORY = 'Creational';

    public function exec()
    {
        $pattern = new PrototypeExample();
        $pattern->setMessage('Описание паттерна '.$this::NAME, 'h4');
        $pattern->setMessage('Классическая схема шаблона Прототип. <br><img src=https://upload.wikimedia.org/wikipedia/commons/a/af/Prototype_design_pattern.png>', 'p');

        $pattern->run();
        $view = new PatternView();
        $view->setBody($pattern->getMessage())->setTitle('Singleton')->render();
    }
}
