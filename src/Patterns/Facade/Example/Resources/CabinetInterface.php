<?php

namespace Patterns\Facade\Example\Resources;

interface CabinetInterface
{
    public function launch();

    public function getReason(): string;

    public function getStatus(): string;

    public function halt();
}
