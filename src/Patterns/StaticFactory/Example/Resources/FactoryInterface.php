<?php

declare(strict_types=1);

namespace Patterns\StaticFactory\Example\Resources;

interface FactoryInterface
{
    public function getCategory();

    public function getExecutor();
}
