<?php

namespace Patterns\SimpleFactory\Controller;

use App\View\PatternView;
use Patterns\SimpleFactory\Example\SimpleFactoryExample;

class SimpleFactoryController
{
    const NAME = 'SimpleFactory';
    const DESCRIPTION = 'Simple Factory';
    const CATEGORY = 'Creational';

    public function exec()
    {
        $pattern = new SimpleFactoryExample();
        $pattern->setMessage('Описание паттерна '.$this::NAME, 'h4');
        $pattern->run();
        $view = new PatternView();
        $view->setBody($pattern->getMessage())->setTitle('Singleton')->render();
    }
}
