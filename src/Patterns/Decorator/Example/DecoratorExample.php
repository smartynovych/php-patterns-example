<?php

namespace Patterns\Decorator\Example;

use App\Services\PatternDescriptionServices;
use Patterns\Decorator\Example\Resources\Windows;
use Patterns\Decorator\Example\Resources\WindowsHorizontalScroll;
use Patterns\Decorator\Example\Resources\WindowsVerticalScroll;

class DecoratorExample extends PatternDescriptionServices
{
    public function run()
    {
        $this->setMessage('Создаем объект Окна', 'strong');
        $this->setMessage('$windows = new Windows(\'Explorer windows\');', 'code');
        $windows = new Windows('Explorer windows');

        $this->setMessage('Отрисовываем окно', 'i');
        $this->setMessage('$this->setMessage($windows->renderWindows());', 'code');
        $this->setMessage($windows->renderWindows());

        $this->setMessage('Добавляем вертикальную прокрутку в окне', 'i');
        $this->setMessage('$windows = new WindowsVerticalScroll($windows);', 'code');
        $windows = new WindowsVerticalScroll($windows);

        $this->setMessage('Перерисовываем окно', 'i');
        $this->setMessage('$this->setMessage($windows->renderWindows());', 'code');
        $this->setMessage($windows->renderWindows());

        $this->setMessage('Добавляем горизонтальную прокрутку в окне', 'i');
        $this->setMessage('$windows = new WindowsHorizontalScroll($windows);', 'code');
        $windows = new WindowsHorizontalScroll($windows);

        $this->setMessage('Перерисовываем окно', 'i');
        $this->setMessage('$this->setMessage($windows->renderWindows());', 'code');
        $this->setMessage($windows->renderWindows());
    }
}
