<?php

declare(strict_types=1);

namespace Patterns\FactoryMethod\Example\Resources;

interface ProductProvider
{
    public function setName(string $name);

    public function setDescription(string $description);

    public function setPrice(float $price);
}
