<?php

namespace Patterns\ObjectPool\Example\Resources;

class BallsPool
{
    /**
     * @var CreateBalls[]
     */
    private $occupiedBalls = [];

    /**
     * @var CreateBalls[]
     */
    private $freeBalls = [];

    public function get(): CreateBalls
    {
        if (0 == count($this->freeBalls)) {
            $ball = new CreateBalls();
        } else {
            $ball = array_pop($this->freeBalls);
        }

        $this->occupiedBalls[spl_object_hash($ball)] = $ball;

        return $ball;
    }

    public function dispose(CreateBalls $ball)
    {
        $key = spl_object_hash($ball);

        if (isset($this->occupiedBalls[$key])) {
            unset($this->occupiedBalls[$key]);
            $this->freeBalls[$key] = $ball;
        }
    }
}
