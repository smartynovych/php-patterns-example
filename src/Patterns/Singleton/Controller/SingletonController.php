<?php

namespace Patterns\Singleton\Controller;

use App\View\PatternView;
use Patterns\Singleton\Example\ConfigExample;

class SingletonController
{
    const NAME = 'Singleton';
    const DESCRIPTION = 'Singleton';
    const CATEGORY = 'Creational';

    public function exec()
    {
        $msg[] = 'Создаем экземпляр класса Config';
        $config = ConfigExample::getInstance();

        $msg[] = 'Пробуем создать новый экземпляр класса Config';
        $config2 = ConfigExample::getInstance();

        $msg[] = 'С помощью обьекта 1 сохраняем в конфиг файл новай параметр singleton со значением object1';
        $parameters = [
          'singleton' => ['name' => 'object1'],
        ];
        $config->setParameter($parameters);

        $parameters = $config->getParameter();
        $msg[] = 'С помощью обьекта 1 проверяем значение параметра singleton, оно равно '.$parameters['singleton']['name'];

        $parameters = $config2->getParameter();
        $msg[] = 'С помощью обьекта 2 проверяем значение параметра singleton, оно так же равно '.$parameters['singleton']['name'];

        $msg[] = 'С помощью обьекта 2 сохраняем в конфиг файл новай параметр singleton со значением object2';
        $parameters = [
            'singleton' => ['name' => 'object2'],
        ];
        $config2->setParameter($parameters);

        $parameters = $config2->getParameter();
        $msg[] = 'С помощью обьекта 2 проверяем значение параметра singleton, оно равно '.$parameters['singleton']['name'];

        $parameters = $config->getParameter();
        $msg[] = 'С помощью обьекта 1 проверяем значение параметра singleton, оно равно '.$parameters['singleton']['name'];

        $msg[] = 'Вывод: с помощью паттерна Singleton можно создать только один экзепляр класса Config';

        $parameters = [
            'singleton' => null,
        ];
        $config->setParameter($parameters);

        $view = new PatternView();
        $view->setBody(implode('<br>', $msg))->setTitle('Singleton')->render();
    }
}
