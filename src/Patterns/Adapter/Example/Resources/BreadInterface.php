<?php

namespace Patterns\Adapter\Example\Resources;

/**
 * Interface BreadInterface.
 */
interface BreadInterface
{
    /**
     * @return int|null
     */
    public function browse(): ?int;

    /**
     * @return int|null
     */
    public function read(): ?int;

    public function edit(): void;

    public function add(): void;

    public function delete(): void;
}
