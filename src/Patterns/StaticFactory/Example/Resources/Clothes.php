<?php

declare(strict_types=1);

namespace Patterns\StaticFactory\Example\Resources;

class Clothes implements FactoryInterface
{
    public function getCategory()
    {
        return 'T-SHIRT';
    }

    public function getExecutor()
    {
        return 'AUNT KLAVA';
    }
}
