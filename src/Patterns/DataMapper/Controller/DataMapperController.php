<?php

namespace Patterns\DataMapper\Controller;

use App\View\PatternView;
use Patterns\DataMapper\Example\DataMapperExample;

class DataMapperController
{
    const NAME = 'DataMapper';
    const DESCRIPTION = 'DataMapper (Преобразователь Данных)';
    const CATEGORY = 'Structural';

    public function exec()
    {
        $pattern = new DataMapperExample();
        $pattern->setMessage('Описание паттерна '.$this::NAME, 'h4');
        $pattern->run();

        $view = new PatternView();
        $view->setBody($pattern->getMessage())->setTitle('Singleton')->render();
    }
}
