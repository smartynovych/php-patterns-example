<?php

namespace Patterns\Bridge\Controller;

use App\View\PatternView;
use Patterns\Bridge\Example\BridgeExample;

class BridgeController
{
    const NAME = 'Bridge';
    const DESCRIPTION = 'Bridge (Мост)';
    const CATEGORY = 'Structural';

    public function exec()
    {
        $pattern = new BridgeExample();
        $pattern->setMessage('Описание паттерна '.$this::NAME, 'h4');
        $pattern->run();

        $view = new PatternView();
        $view->setBody($pattern->getMessage())->setTitle('Singleton')->render();
    }
}
