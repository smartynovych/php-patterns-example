<?php

namespace Patterns\Composite\Example\Resources;

class Cavalry implements RenderableInterface
{
    private $count;

    public function __construct(int $count)
    {
        $this->count = $count;
    }

    public function render(): string
    {
        return 'Cavalry: '.$this->count.' units';
    }
}
