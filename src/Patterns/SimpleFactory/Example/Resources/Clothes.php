<?php

declare(strict_types=1);

namespace Patterns\SimpleFactory\Example\Resources;

class Clothes
{
    public function TShirt(string $type = ''): string
    {
        return 'T-SHIRT'.$type;
    }

    public function Pants(string $type = ''): string
    {
        return 'PANTS'.$type;
    }
}
