<?php

namespace Patterns\Decorator\Example\Resources;

interface WindowsInterface
{
    public function renderWindows(): string;
}
