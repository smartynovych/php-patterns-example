<?php

namespace Patterns\Facade\Example\Resources;

class Locker
{
    private $cabinet;

    private $api;

    public function __construct(CabinetInterface $cabinet, ApiInterface $api)
    {
        $this->cabinet = $cabinet;
        $this->api = $api;
    }

    public function lock()
    {
        $this->api->deny($this->cabinet);
        $this->cabinet->halt();
    }

    public function unLock()
    {
        $this->cabinet->launch();
        $this->api->allow($this->cabinet);
        $this->api->check();
    }
}
