<?php

declare(strict_types=1);

namespace Patterns\AbstractFactory\Example\Resources;

class AdultFactory extends AbstractFactory
{
    public function createToys(string $content): ProviderToys
    {
        return new AdultToys($content);
    }

    public function createBooks(string $content): ProviderBooks
    {
        return new AdultBooks($content);
    }

    public function createClothes(string $content): ProviderClothes
    {
        return new AdultClothes($content);
    }
}
