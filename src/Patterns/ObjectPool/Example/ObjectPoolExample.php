<?php

namespace Patterns\ObjectPool\Example;

use App\Services\PatternDescriptionServices;
use Patterns\ObjectPool\Example\Resources\BallsPool;

class ObjectPoolExample extends PatternDescriptionServices
{
    public function run()
    {
        $this->setMessage('Создаем пул для работы с объектами Мячики', 'strong');
        $this->setMessage('$pool = new BallsPool();', 'code');

        $pool = new BallsPool();

        $this->setMessage('Получаем первый объект из пула', 'i');
        $this->setMessage('$ball1 = $pool->get();', 'code');
        $ball1 = $pool->get();

        $this->setMessage('Свойства и внешний вид объекта $ball1', 'i');
        $this->setMessage('$this->showBall($ball1->getPosition(), $ball1->getDiagonal(), $ball1->getColor());', 'code');
        $this->showBall($ball1->getPosition(), $ball1->getDiagonal(), $ball1->getColor());

        $this->setMessage('Получаем второй объект из пула', 'i');
        $this->setMessage('$ball2 = $pool->get();', 'code');
        $ball2 = $pool->get();

        $this->setMessage('Свойства и внешний вид объекта $ball2', 'i');
        $this->setMessage('$this->showBall($ball2->getPosition(), $ball2->getDiagonal(), $ball2->getColor());', 'code');
        $this->showBall($ball2->getPosition(), $ball2->getDiagonal(), $ball2->getColor());

        $this->setMessage('Освобождаем пул от объекта $ball1', 'i');
        $this->setMessage('$pool->dispose($ball1);', 'code');
        $pool->dispose($ball1);

        $this->setMessage('Освобождаем пул от объекта $ball2', 'i');
        $this->setMessage('$pool->dispose($ball2);', 'code');
        $pool->dispose($ball2);

        $this->setMessage('Получаем следующий объект из пула', 'i');
        $this->setMessage('$ball3 = $pool->get();', 'code');
        $ball3 = $pool->get();

        $this->setMessage('Свойства и внешний вид объекта $ball3 идетничек объекту $ball2 который мы освободили ранее', 'i');
        $this->setMessage('$this->showBall($ball3->getPosition(), $ball3->getDiagonal(), $ball3->getColor());', 'code');
        $this->showBall($ball3->getPosition(), $ball3->getDiagonal(), $ball3->getColor());

        $this->setMessage('Получаем следующий объект из пула', 'i');
        $this->setMessage('$ball4 = $pool->get();', 'code');
        $ball4 = $pool->get();

        $this->setMessage('Свойства и внешний вид объекта $ball4 идетничек объекту $ball1 который мы освободили ранее', 'i');
        $this->setMessage('$this->showBall($ball4->getPosition(), $ball4->getDiagonal(), $ball4->getColor());', 'code');
        $this->showBall($ball4->getPosition(), $ball4->getDiagonal(), $ball4->getColor());

        $this->setMessage('Получаем следующий объект из пула', 'i');
        $this->setMessage('$ball5 = $pool->get();', 'code');
        $ball5 = $pool->get();

        $this->setMessage('Свойства и внешний вид объекта $ball5 отличается от ранее полученных поэтому является новым', 'i');
        $this->setMessage('$this->showBall($ball5->getPosition(), $ball5->getDiagonal(), $ball5->getColor());', 'code');
        $this->showBall($ball5->getPosition(), $ball5->getDiagonal(), $ball5->getColor());
    }

    private function showBall($position, $diagonal, $color)
    {
        $this->setMessage(sprintf('<span>COLOR: %s; POSITION: %d; DIAGONAL: %d; </span>
<div style="margin-left: %dpx; width: %dpx; height: %dpx; background-color: %s;" class="rounded-circle">
</div>', $color, $position, $diagonal, $position, $diagonal, $diagonal, $color), 'div');
    }
}
