<?php

namespace Patterns\Bridge\Example\Resources;

class Cafe extends DrinkAbstract
{
    public function get()
    {
        return $this->implementation->create('Cafe');
    }
}
