<?php

use PHPUnit\Framework\TestCase;
use Patterns\Bridge\Example\Resources\Tea;
use Patterns\Bridge\Example\Resources\Cafe;
use Patterns\Bridge\Example\Resources\Sugar;
use Patterns\Bridge\Example\Resources\Milk;

class BridgeTest extends TestCase
{
    public function testBridge()
    {
        $sugar = new Sugar();
        $tea = new Tea($sugar);
        $this->assertEquals('Tea with sugar', $tea->get());

        $milk = new Milk();
        $tea->setImplementation($milk);
        $this->assertEquals('Tea with milk', $tea->get());

        $cafe = new Cafe($sugar);
        $this->assertEquals('Cafe with sugar', $cafe->get());

        $cafe->setImplementation($milk);
        $this->assertEquals('Cafe with milk', $cafe->get());
    }
}
