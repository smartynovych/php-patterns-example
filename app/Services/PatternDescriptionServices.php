<?php

namespace App\Services;

class PatternDescriptionServices
{
    protected $message = [];

    /**
     * @param mixed $message
     */
    public function setMessage($message, $tag = 'br')
    {
        switch ($tag) {
            case 'p':
                $this->message[] = '<p>'.$message.'</p>';
                break;

            case 'br':
                $this->message[] = '<br>> '.$message.'<br>';
                break;

            case 'hr':
                $this->message[] = '<br><hr>';
                break;

            case 'wiki':
                $this->message[] = '<div style="float:right;"><a href="'.$message.'" target="_blank"><img src="/images/wiki.png" width="64"></a></div>';
                break;

            case '':
                $this->message[] = '<br>'.$message.'<br>';
                break;

            default:
                $this->message[] = '<br><'.$tag.'>'.$message.'</'.$tag.'><br>';
                break;
        }
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return implode('', $this->message);
    }
}
