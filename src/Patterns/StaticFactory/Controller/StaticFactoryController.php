<?php

namespace Patterns\StaticFactory\Controller;

use App\View\PatternView;
use Patterns\StaticFactory\Example\StaticFactoryExample;

class StaticFactoryController
{
    const NAME = 'StaticFactory';
    const DESCRIPTION = 'Static Factory';
    const CATEGORY = 'Creational';

    public function exec()
    {
        $pattern = new StaticFactoryExample();
        $pattern->setMessage('Описание паттерна '.$this::NAME, 'h4');
        $pattern->run();
        $view = new PatternView();
        $view->setBody($pattern->getMessage())->setTitle('Singleton')->render();
    }
}
