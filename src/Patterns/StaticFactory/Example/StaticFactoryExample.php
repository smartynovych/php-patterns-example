<?php

namespace Patterns\StaticFactory\Example;

use Patterns\StaticFactory\Example\Resources\StaticFactory;
use App\Services\PatternDescriptionServices;

class StaticFactoryExample extends PatternDescriptionServices
{
    public function run()
    {
        $this->setMessage('Создаем фабрику по производству Игрушек', 'p');
        $this->setMessage('$toys = StaticFactory::factory(\'toys\');', 'code');
        $toys = StaticFactory::factory('toys');

        $this->setMessage('Получает категорию игрушек', 'i');
        $this->setMessage('$toys->getCategory();', 'code');
        $this->setMessage($toys->getCategory());

        $this->setMessage('Получает создателя игрушек', 'i');
        $this->setMessage('$toys->getExecutor();', 'code');
        $this->setMessage($toys->getExecutor());

        $this->setMessage('', 'hr');

        $this->setMessage('Создаем фабрику по производству Книг', 'p');
        $this->setMessage('$books = StaticFactory::factory(\'books\');', 'code');
        $books = StaticFactory::factory('books');

        $this->setMessage('Получает категорию книг', 'i');
        $this->setMessage('$books->getCategory();', 'code');
        $this->setMessage($books->getCategory());

        $this->setMessage('Получает создателя книг', 'i');
        $this->setMessage('$books->getExecutor();', 'code');
        $this->setMessage($books->getExecutor());

        $this->setMessage('', 'hr');

        $this->setMessage('Создаем фабрику по производству Одежды', 'p');
        $this->setMessage('$clothes = StaticFactory::factory(\'clothes\');', 'code');
        $clothes = StaticFactory::factory('clothes');

        $this->setMessage('Получает категорию одежды', 'i');
        $this->setMessage('$clothes->getCategory());', 'code');
        $this->setMessage($clothes->getCategory());

        $this->setMessage('Получает создателя одежды', 'i');
        $this->setMessage('$clothes->getExecutor();', 'code');
        $this->setMessage($clothes->getExecutor());
    }
}
