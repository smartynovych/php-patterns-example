<?php

use PHPUnit\Framework\TestCase;
use Patterns\DataMapper\Example\Resources\StorageAdapter;
use Patterns\DataMapper\Example\Resources\UserMapper;
use Patterns\DataMapper\Example\Resources\User;

class DataMapperTest extends TestCase
{
    public function testDataMapper()
    {
        $storage = new StorageAdapter([1 => ['username' => 'user', 'email' => 'username@mail.com']]);
        $mapper = new UserMapper($storage);

        $user = $mapper->findById(1);

        $this->assertInstanceOf(User::class, $user);
    }
}
