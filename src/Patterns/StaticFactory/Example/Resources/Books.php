<?php

declare(strict_types=1);

namespace Patterns\StaticFactory\Example\Resources;

class Books implements FactoryInterface
{
    public function getCategory()
    {
        return 'FANTASY';
    }

    public function getExecutor()
    {
        return 'AUNT ROWLING';
    }
}
