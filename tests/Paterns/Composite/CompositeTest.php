<?php

use PHPUnit\Framework\TestCase;
use Patterns\Composite\Example\Resources\Army;
use Patterns\Composite\Example\Resources\Cavalry;
use Patterns\Composite\Example\Resources\Infantry;
use Patterns\Composite\Example\Resources\Sheep;

class CompositeTest extends TestCase
{
    public function testComposite()
    {
        $army = new Army();
        $army->addElement(new Cavalry(1000));
        $army->addElement(new Infantry(5000));
        $army->render();

        $this->assertEquals(
            '<ol><li>Cavalry: 1000 units</li><li>Infantry: 5000 units</li></ol>',
            $army->render()
        );

        $fleet = new Army();
        $fleet->addElement(new Sheep(50));
        $army->addElement($fleet);
        $this->assertEquals(
            '<ol><li>Cavalry: 1000 units</li><li>Infantry: 5000 units</li><li><ol><li>Sheep: 50 units</li></ol></li></ol>',
            $army->render()
        );
    }
}
