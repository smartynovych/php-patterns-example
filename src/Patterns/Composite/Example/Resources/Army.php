<?php

namespace Patterns\Composite\Example\Resources;

class Army implements RenderableInterface
{
    /**
     * @var RenderableInterface[]
     */
    private $elements;

    /**
     * @return string
     */
    public function render(): string
    {
        $list = '<ol>';

        foreach ($this->elements as $element) {
            $list .= '<li>';
            $list .= $element->render();
            $list .= '</li>';
        }

        $list .= '</ol>';

        return $list;
    }

    /**
     * @param RenderableInterface $element
     */
    public function addElement(RenderableInterface $element)
    {
        $this->elements[] = $element;
    }
}
