<?php

use PHPUnit\Framework\TestCase;
use Patterns\Adapter\Example\Resources\Crud;
use Patterns\Adapter\Example\Resources\Bread;
use Patterns\Adapter\Example\Resources\BreadAdapter;

class AdapterTest extends TestCase
{
    public function testCrud()
    {
        $trigger = new Crud();
        $trigger->create();
        $this->assertEquals(0, $trigger->read());
        $trigger->update();
        $this->assertEquals(1, $trigger->read());
    }

    public function testAdapter()
    {
        $breadTrigger = new Bread();
        $trigger = new BreadAdapter($breadTrigger);
        $trigger->create();
        $this->assertEquals(0, $trigger->read());
        $trigger->update();
        $this->assertEquals(1, $trigger->read());
    }
}
