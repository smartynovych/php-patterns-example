<?php

namespace Patterns\AbstractFactory\Controller;

use App\View\PatternView;
use Patterns\AbstractFactory\Example\AbstractFactoryExample;

class AbstractFactoryController
{
    const NAME = 'AbstractFactory';
    const DESCRIPTION = 'Abstract Factory';
    const CATEGORY = 'Creational';

    public function exec()
    {
        $pattern = new AbstractFactoryExample();
        $pattern->setMessage('Описание паттерна '.$this::NAME, 'h4');
        $pattern->run();
        $view = new PatternView();
        $view->setBody($pattern->getMessage())->setTitle('Singleton')->render();
    }
}
