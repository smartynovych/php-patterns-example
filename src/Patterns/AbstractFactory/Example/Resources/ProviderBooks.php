<?php

declare(strict_types=1);

namespace Patterns\AbstractFactory\Example\Resources;

abstract class ProviderBooks
{
    abstract public function getName(): string;

    abstract public function getDescription(): string;

    abstract public function getPrice(): string;
}
