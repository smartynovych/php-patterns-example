<?php

namespace Patterns\Facade\Example;

use App\Services\PatternDescriptionServices;
use Patterns\Facade\Example\Resources\Crud;

class FacadeExample extends PatternDescriptionServices
{
    public function run()
    {
    }
}
