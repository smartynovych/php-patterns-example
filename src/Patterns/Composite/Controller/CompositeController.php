<?php

namespace Patterns\Composite\Controller;

use App\View\PatternView;
use Patterns\Composite\Example\CompositeExample;

class CompositeController
{
    const NAME = 'Composite';
    const DESCRIPTION = 'Composite (Компоновщик)';
    const CATEGORY = 'Structural';

    public function exec()
    {
        $pattern = new CompositeExample();
        $pattern->setMessage('Описание паттерна '.$this::NAME, 'h4');
        $pattern->run();

        $view = new PatternView();
        $view->setBody($pattern->getMessage())->setTitle('Singleton')->render();
    }
}
