<?php

namespace Patterns\Prototype\Example\Resources;

class Ball extends FigurePrototype
{
    protected $type = 'ball';

    public function __clone()
    {
    }
}
