<?php

namespace Patterns\Builder\Example\Resources;

class WarlandBuilder extends CountryBuilder
{
    public function addName()
    {
        $this->country->setName('WarLand');
    }

    public function addPopulation()
    {
        $this->country->setPopulation(100000);
    }

    public function addLanguage()
    {
        $this->country->setLanguage('warious');
    }

    public function addFeature()
    {
        $this->country->setFeature('War');
    }

    /*

    public function createCountry()
    {
        $this->warland = new Country();
    }

    public function getCountry()
    {
        return $this->warland;
    }*/
}
