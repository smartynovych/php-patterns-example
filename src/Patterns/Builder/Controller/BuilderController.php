<?php

namespace Patterns\Builder\Controller;

use App\View\PatternView;
use Patterns\Builder\Example\BuilderExample;

class BuilderController
{
    const NAME = 'Builder';
    const DESCRIPTION = 'Builder';
    const CATEGORY = 'Creational';

    public function exec()
    {
        $pattern = new BuilderExample();
        $pattern->setMessage('Описание паттерна '.$this::NAME, 'h4');
        $pattern->setMessage('Классическая схема шаблона Строитель. <br><img src=https://upload.wikimedia.org/wikipedia/commons/9/94/Builder_design_pattern.png>', 'p');
        $pattern->run();
        $view = new PatternView();
        $view->setBody($pattern->getMessage())->setTitle('Singleton')->render();
    }
}
