<?php

declare(strict_types=1);

namespace Patterns\AbstractFactory\Example\Resources;

abstract class AbstractFactory
{
    abstract public function createToys(string $content): ProviderToys;

    abstract public function createBooks(string $content): ProviderBooks;

    abstract public function createClothes(string $content): ProviderClothes;
}
