<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

date_default_timezone_set('Europe/Kiev');

require __DIR__.'/../vendor/autoload.php';

$project = new Router();
$project->start();
