<?php

namespace App\View;

/**
 * Class PatternView.
 */
class PatternView
{
    /**
     * @var string
     */
    private $template = '';
    /**
     * @var string
     */
    private $title = '';
    /**
     * @var string
     */
    private $body = '';
    /**
     * @var string
     */
    private $patternList = '';

    /**
     * PatternView constructor.
     *
     * @param string $template
     */
    public function __construct($template = 'index')
    {
        $this->setTemplate($template);
    }

    public function render()
    {
        include $_SERVER['DOCUMENT_ROOT'].'/../app/Resources/view/'.$this->getTemplate().'.view.php';
    }

    /**
     * @return mixed
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @param mixed $template
     */
    public function setTemplate($template): self
    {
        $this->template = mb_strtolower($template);

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getBody(): string
    {
        return $this->body;
    }

    /**
     * @param string $body
     */
    public function setBody(string $body): self
    {
        $this->body = $body;

        return $this;
    }

    /**
     * @return string
     */
    public function getPatternList(): string
    {
        return $this->patternList;
    }

    /**
     * @param array $patternList
     */
    public function setPatternList(array $patternList): self
    {
        $html = '';
        foreach ($patternList as $category => $value) {
            $html .= '<ul class="list-group">';
            $html .= '<li class="list-group-item active disabled">'.mb_strtoupper($category).'</li>';
            foreach ($value as $pattern => $name) {
                $html .= '<li class="list-group-item"><a href="/'.$pattern.'">'.$name.'</a></li>';
            }
            $html .= '</ul><br>';
        }

        $this->patternList = $html;

        return $this;
    }
}
