<?php

declare(strict_types=1);

namespace Patterns\SimpleFactory\Example\Resources;

class SimpleFactory
{
    public function CreateToys(): Toys
    {
        return new Toys();
    }

    public function CreateBooks(): Books
    {
        return new Books();
    }

    public function CreateClothes(): Clothes
    {
        return new Clothes();
    }
}
