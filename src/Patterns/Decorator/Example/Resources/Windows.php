<?php

namespace Patterns\Decorator\Example\Resources;

class Windows implements WindowsInterface
{
    private $windowsData;

    public function __construct($windowsData)
    {
        $this->windowsData = $windowsData;
    }

    public function renderWindows(): string
    {
        return $this->windowsData;
    }
}
