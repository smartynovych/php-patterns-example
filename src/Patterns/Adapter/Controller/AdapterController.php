<?php

namespace Patterns\Adapter\Controller;

use App\View\PatternView;
use Patterns\Adapter\Example\AdapterExample;

class AdapterController
{
    const NAME = 'Adapter';
    const DESCRIPTION = 'Adapter (Адаптер / Wrapper)';
    const CATEGORY = 'Structural';

    public function exec()
    {
        $pattern = new AdapterExample();
        $pattern->setMessage('Описание паттерна '.$this::NAME, 'h4');
        $pattern->run();

        $view = new PatternView();
        $view->setBody($pattern->getMessage())->setTitle('Singleton')->render();
    }
}
