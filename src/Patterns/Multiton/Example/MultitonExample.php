<?php

namespace Patterns\Multiton\Example;

use App\Services\PatternDescriptionServices;
use Patterns\Multiton\Example\Resources\Multiton;

class MultitonExample extends PatternDescriptionServices
{
    public function run()
    {
        $this->setMessage('Создаем соединение с MySQL', 'strong');
        $this->setMessage('$db = Multiton::getInstance(Multiton::MYSQL);', 'code');
        $db = Multiton::getInstance(Multiton::MYSQL);

        $this->setMessage('Получаем параметр заданный для этой одиночки', 'i');
        $this->setMessage('$db->getConnection();', 'code');
        $this->setMessage($db->getConnection());

        $this->setMessage('Создаем одиночку SQLite', 'strong');
        $this->setMessage('$db = Multiton::getInstance(Multiton::SQLITE);', 'code');
        $db = Multiton::getInstance(Multiton::SQLITE);

        $this->setMessage('Получаем параметр заданный для этой одиночки', 'i');
        $this->setMessage('$db->getConnection();', 'code');
        $this->setMessage($db->getConnection());
    }
}
