<?php

declare(strict_types=1);

namespace Patterns\StaticFactory\Example\Resources;

class Toys implements FactoryInterface
{
    public function getCategory()
    {
        return 'LEGO';
    }

    public function getExecutor()
    {
        return 'UNCLE DUPLO';
    }
}
