<?php

namespace Patterns\Decorator\Example\Resources;

abstract class WindowsDecorator implements WindowsInterface
{
    /**
     * @var WindowsInterface
     */
    protected $wrapped;

    /**
     * @param WindowsInterface $renderer
     */
    public function __construct(WindowsInterface $renderer)
    {
        $this->wrapped = $renderer;
    }
}
