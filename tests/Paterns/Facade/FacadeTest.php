<?php

use PHPUnit\Framework\TestCase;
use Patterns\Facade\Example\Resources\Locker;

class FacadeTest extends TestCase
{
    public function testFacadeDeny()
    {
        $cabinet = $this->createMock('Patterns\Facade\Example\Resources\CabinetInterface');

        $cabinet->method('getReason')
            ->will($this->returnValue('System update'));

        $api = $this->getMockBuilder('Patterns\Facade\Example\Resources\ApiInterface')
            ->setMethods(['deny'])
            ->disableAutoload()
            ->getMock();

        $api->expects($this->once())
            ->method('deny')
            ->with($cabinet);

        $facade = new Locker($cabinet, $api);

        // the facade interface is simple
        $facade->lock();

        // but you can also access the underlying components
        $this->assertEquals('System update', $cabinet->getReason());
    }

    public function testFacadeAllow()
    {
        $cabinet = $this->createMock('Patterns\Facade\Example\Resources\CabinetInterface');

        $cabinet->method('getStatus')
            ->will($this->returnValue('System success'));

        $api = $this->getMockBuilder('Patterns\Facade\Example\Resources\ApiInterface')
            ->setMethods(['allow', 'check'])
            ->disableAutoload()
            ->getMock();

        $facade = new Locker($cabinet, $api);

        // the facade interface is simple
        $facade->unlock();

        // but you can also access the underlying components
        $this->assertEquals('System success', $cabinet->getStatus());
    }
}
