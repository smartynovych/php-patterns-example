<?php

namespace Patterns\Singleton\Example;

use Symfony\Component\Yaml\Yaml;

class ConfigExample
{
    /**
     * @var ConfigExample
     */
    private static $instance;

    /**
     * @var Yaml Parameters
     */
    private $parameters;

    private function __construct()
    {
        $this->parameters = Yaml::parse(file_get_contents(__DIR__.'/../../../../app/config/parameters.yml'));
    }

    public static function getInstance(): self
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    public function getParameter()
    {
        return $this->parameters;
    }

    public function setParameter($updates)
    {
        $parameters = array_merge(Yaml::parse(file_get_contents(__DIR__.'/../../../../app/config/parameters.yml')), $updates);

        $yaml = Yaml::dump($parameters);
        if (file_put_contents(__DIR__.'/../../../../app/config/parameters.yml', $yaml)) {
            $this->parameters = Yaml::parse(file_get_contents(__DIR__.'/../../../../app/config/parameters.yml'));
        }
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    /*    public function __destruct()
        {
            $this->setParameter([
                'singleton' => null]);
        }*/
}
