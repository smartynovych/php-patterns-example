<?php

namespace Patterns\Adapter\Example\Resources;

/**
 * Class Crud.
 */
class Crud implements CrudInterface
{
    /**
     * @var null
     */
    private $bit = null;

    public function create(): void
    {
        $this->bit = 0;
    }

    /**
     * @return int
     */
    public function read(): ?int
    {
        return $this->bit;
    }

    public function update(): void
    {
        $this->bit = 1;
    }

    public function delete(): void
    {
        $this->bit = null;
    }
}
