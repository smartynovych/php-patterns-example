<?php

namespace Patterns\FactoryMethod\Example;

use App\Services\PatternDescriptionServices;
use Patterns\FactoryMethod\Example\Resources\ChildrenFactory;
use Patterns\FactoryMethod\Example\Resources\FactoryMethod;

class FactoryMethodExample extends PatternDescriptionServices
{
    public function run()
    {
        $this->setMessage('Создаем фабрику товаров для Детей', 'strong');
        $this->setMessage('$factory = new ChildrenFactory();', 'code');
        $factory = new ChildrenFactory();

        $this->setMessage('Будем создавать игрушки', 'i');
        $this->setMessage('$toys = $factory->create(FactoryMethod::TOYS);', 'code');
        $toys = $factory->create(FactoryMethod::TOYS);

        $this->setMessage('Производим игрушку LEGO Train по цене 9.99', 'i');
        $this->setMessage('$toys->setName(\'LEGO\');', 'code');
        $toys->setName('LEGO');
        $this->setMessage('$toys->setDescription(\'Train\');', 'code');
        $toys->setDescription('Train');
        $this->setMessage('$toys->setPrice(9.99);', 'code');
        $toys->setPrice(9.99);

        $this->setMessage('На выходе получаем информацию о произведенной игрушке:', 'i');
        $this->setMessage('$toys->getName()', 'code');
        $this->setMessage($toys->getName());
        $this->setMessage('$toys->getDescription()', 'code');
        $this->setMessage($toys->getDescription());
        $this->setMessage('$toys->getPrice()', 'code');
        $this->setMessage($toys->getPrice());
    }
}
