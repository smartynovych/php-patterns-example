<?php

use PHPUnit\Framework\TestCase;
use Patterns\Builder\Example\Resources\Lordship;
use Patterns\Builder\Example\Resources\WarlandBuilder;
use Patterns\Builder\Example\Resources\FarmerlandBuilder;

class BuilderTest extends TestCase
{
    public function testBuilder()
    {
        $newCountry = new Lordship();
        $warland = new WarlandBuilder();
        $this->assertContains('WARLAND', $newCountry->build($warland)->getName());
        $farmerland = new FarmerlandBuilder();
        $this->assertContains('FARMERLAND', $newCountry->build($farmerland)->getName());
    }
}
