<?php

declare(strict_types=1);

namespace Patterns\FactoryMethod\Example\Resources;

class AdultFactory extends FactoryMethod
{
    public function createProduct(string $productType): ProductProvider
    {
        switch ($productType) {
            case parent::BOOKS:
                return new Books();
                break;

            case parent::CLOTHES:
                return new Clothes();
                break;

            default:
                throw new \InvalidArgumentException("$productType is not a valid product for adults");
                break;
        }
    }
}
