<?php

namespace Patterns\AbstractFactory\Example;

use App\Services\PatternDescriptionServices;
use Patterns\AbstractFactory\Example\Resources\ChildrenFactory;

class AbstractFactoryExample extends PatternDescriptionServices
{
    public function run()
    {
        $this->setMessage('Создаем фабрику товаров для Детей', 'strong');
        $this->setMessage('$factory = new ChildrenFactory();', 'code');
        $factory = new ChildrenFactory();

        $this->setMessage('Отправляем запрос на создание игрушки LEGO ', 'i');
        $this->setMessage('$toys = $factory->createToys(\'LEGO\');', 'code');

        $toys = $factory->createToys('LEGO');

        $this->setMessage('На выходе получаем информацию о произведенной игрушке:', 'i');
        $this->setMessage('$toys->getName()', 'code');
        $this->setMessage($toys->getName());
        $this->setMessage('$toys->getDescription()', 'code');
        $this->setMessage($toys->getDescription());
        $this->setMessage('$toys->getPrice()', 'code');
        $this->setMessage($toys->getPrice());

        $this->setMessage('', 'hr');

        $this->setMessage('Создаем фабрику товаров для Взрослых', 'strong');
        $this->setMessage('$factory = new AdultFactory();', 'code');
        $factory = new AdultFactory();

        $this->setMessage('Отправляем запрос на создание одежды Футболка Adidas', 'i');
        $this->setMessage('$cloches = $factory->createClothes(\'Футболка Adidas\');', 'code');

        $cloches = $factory->createClothes('Футболка Adidas');

        $this->setMessage('На выходе получаем информацию о произведенной одежде:', 'i');
        $this->setMessage('$cloches->getName()', 'code');
        $this->setMessage($cloches->getName());
        $this->setMessage('$cloches->getDescription()', 'code');
        $this->setMessage($cloches->getDescription());
        $this->setMessage('$cloches->getPrice()', 'code');
        $this->setMessage($cloches->getPrice());
    }
}
