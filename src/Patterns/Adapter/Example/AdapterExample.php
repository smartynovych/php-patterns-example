<?php

namespace Patterns\Adapter\Example;

use App\Services\PatternDescriptionServices;
use Patterns\Adapter\Example\Resources\Bread;
use Patterns\Adapter\Example\Resources\BreadAdapter;
use Patterns\Adapter\Example\Resources\Crud;

class AdapterExample extends PatternDescriptionServices
{
    public function run()
    {
        $this->setMessage('Создаем объект триггер который является экзеплятором класса CRUD', 'strong');
        $this->setMessage('$trigger = new Crud();', 'code');
        $trigger = new Crud();

        $this->setMessage('Создаем триггер', 'i');
        $this->setMessage('$trigger->create();', 'code');
        $trigger->create();

        $this->setMessage('Читаем состояние триггера', 'i');
        $this->setMessage('$trigger->read();', 'code');
        $this->setMessage($trigger->read());
        $trigger->read();

        $this->setMessage('Щелкаем триггером', 'i');
        $this->setMessage('$trigger->update();', 'code');
        $trigger->update();

        $this->setMessage('Читаем состояние триггера', 'i');
        $this->setMessage('$trigger->read();', 'code');
        $this->setMessage($trigger->read());
        $trigger->read();

        $this->setMessage('Удаляем триггер', 'i');
        $this->setMessage('$trigger->delete();', 'code');
        $trigger->delete();

        $this->setMessage('Создаем объект триггер который является экзеплятором класса BREAD', 'strong');
        $this->setMessage('$breadTrigger = new Bread();', 'code');
        $breadTrigger = new Bread();

        $this->setMessage('Подключаем адаптер BreadAdapter', 'i');
        $this->setMessage('$trigger = new BreadAdapter($breadTrigger);', 'code');
        $trigger = new BreadAdapter($breadTrigger);

        $this->setMessage('Создаем триггер', 'i');
        $this->setMessage('$trigger->create();', 'code');
        $trigger->create();

        $this->setMessage('Читаем состояние триггера', 'i');
        $this->setMessage('$trigger->read();', 'code');
        $this->setMessage($trigger->read());
        $trigger->read();

        $this->setMessage('Щелкаем триггером', 'i');
        $this->setMessage('$trigger->update();', 'code');
        $trigger->update();

        $this->setMessage('Читаем состояние триггера', 'i');
        $this->setMessage('$trigger->read();', 'code');
        $this->setMessage($trigger->read());
        $trigger->read();

        $this->setMessage('Удаляем триггер', 'i');
        $this->setMessage('$trigger->delete();', 'code');
        $trigger->delete();
    }
}
