<?php

namespace Patterns\Builder\Example\Resources;

class Lordship
{
    public function build(CountryBuilder $builder): Country
    {
        $builder->createCountry();
        $builder->addName();
        $builder->addPopulation();
        $builder->addLanguage();
        $builder->addFeature();

        return $builder->getCountry();
    }
}
