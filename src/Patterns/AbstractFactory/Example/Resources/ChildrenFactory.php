<?php

declare(strict_types=1);

namespace Patterns\AbstractFactory\Example\Resources;

class ChildrenFactory extends AbstractFactory
{
    public function createToys(string $content): ProviderToys
    {
        return new ChildrenToys($content);
    }

    public function createBooks(string $content): ProviderBooks
    {
        return new ChildrenBooks($content);
    }

    public function createClothes(string $content): ProviderClothes
    {
        return new ChildrenClothes($content);
    }
}
