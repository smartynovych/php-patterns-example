<?php

namespace Patterns\Bridge\Example\Resources;

class Milk implements SupplementsInterface
{
    public function create(string $name)
    {
        return $name.' with milk';
    }
}
