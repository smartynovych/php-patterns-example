<?php

namespace Patterns\Composite\Example\Resources;

class Sheep implements RenderableInterface
{
    private $count;

    public function __construct(int $count)
    {
        $this->count = $count;
    }

    public function render(): string
    {
        return 'Sheep: '.$this->count.' units';
    }
}
