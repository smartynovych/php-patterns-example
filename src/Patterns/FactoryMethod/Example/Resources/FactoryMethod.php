<?php

declare(strict_types=1);

namespace Patterns\FactoryMethod\Example\Resources;

abstract class FactoryMethod
{
    const TOYS = 'toys';
    const BOOKS = 'books';
    const CLOTHES = 'clothes';

    abstract public function createProduct(string $productType): ProductProvider;

    public function create(string $productType): ProductProvider
    {
        return $this->createProduct($productType);
    }
}
