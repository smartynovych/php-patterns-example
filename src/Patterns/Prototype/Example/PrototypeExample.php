<?php

namespace Patterns\Prototype\Example;

use App\Services\PatternDescriptionServices;
use Patterns\Prototype\Example\Resources\Ball;
use Patterns\Prototype\Example\Resources\Square;

class PrototypeExample extends PatternDescriptionServices
{
    public function run()
    {
        $colorList = ['aqua', 'black', 'blue', 'fuchsia', 'gray', 'green',
            'lime', 'maroon', 'navy', 'olive', 'orange', 'purple', 'red',
            'silver', 'teal', 'white', 'yellow', ];

        $this->setMessage('Создаем прототип объекта Мячи', 'strong');
        $this->setMessage('$ballPrototype = new Ball();', 'code');
        $ballPrototype = new Ball();

        $this->setMessage('Создаем прототип объекта Квадраты', 'strong');
        $this->setMessage('$squarePrototype = new Square();', 'code');
        $squarePrototype = new Square();

        $this->setMessage('Теперь можно создавать нужное нам количество клонов и задавать им нужные значения', 'i');
        $this->setMessage('foreach ($colorList as $color) {<br>
            $ball[$color] = clone $ballPrototype;<br>
            $ball[$color]->setColor($color);<br>
<br>
            $square[$color] = clone $squarePrototype;<br>
            $square[$color]->setColor($color);<br>
        }', 'code');

        foreach ($colorList as $color) {
            $ball[$color] = clone $ballPrototype;
            $ball[$color]->setColor($color);

            $square[$color] = clone $squarePrototype;
            $square[$color]->setColor($color);
        }

        $this->setMessage('Получаем свойства созданных объектов', 'i');
        $this->setMessage('$ball[\'green\']->getType();', 'code');
        $this->setMessage($ball['green']->getType());
        $this->setMessage('$ball[\'green\']->getColor();', 'code');
        $this->setMessage($ball['green']->getColor());
        $this->setMessage('$ball[\'maroon\']->getType();', 'code');
        $this->setMessage($square['maroon']->getType());
        $this->setMessage('$ball[\'maroon\']->getColor();', 'code');
        $this->setMessage($square['maroon']->getColor());
    }
}
