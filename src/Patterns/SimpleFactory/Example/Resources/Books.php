<?php

declare(strict_types=1);

namespace Patterns\SimpleFactory\Example\Resources;

class Books
{
    public function Fantasy(string $type = ''): string
    {
        return 'FANTASY'.$type;
    }

    public function Detective(string $type = ''): string
    {
        return 'DETECTIVE'.$type;
    }
}
